package com.example.submoduletestproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mysubmodule.MySubModule

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MySubModule.cool()
    }
}