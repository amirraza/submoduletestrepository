package com.example.mysubmodule

import android.util.Log

object MySubModule {
    fun cool() {
        Log.d("TAGGGG", "Cooooooool from MySubModule")
    }
    fun awesome() = Log.d("TAGGG", "Awesome from MySubModule")
}